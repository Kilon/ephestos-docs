"""
IGNORE:
/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */
 IGNORE:


The module that handles textures

.. inheritance-diagram:: ephpy.hpy.texture
    :parts: 1

"""



from ephpy import pylivecoding
from . import core


def testtlg():
    x = TextureLayersGroup()
    x["but1"]={"visible": True}
    x["but2"]={"visible":True, "active":True}
    return x

# Texture is the images a morph used to display itself, each texture can have one
# or multiple layers visible, allowing for great flexibility into making customised addons.
# Please note that it does not matter if you make multiple textures with layers using same images
# this is because the Hecate ACL (Aura Component Library) not only is coded in C for fast performance
# but also wont allow the same image to be loaded in memory multiple times to optimise drawing and 
# of course memory consumption 
class TextureLayersGroup(pylivecoding.LiveObject):
    """ Instances of TextureLayersGroup are groups of png images represented as a dictionary with properties for name, visibility, animation and activity. Usually it expects a format of a dict {'name':<usually name of png file>, 'visible':<whether the layer is visible or not , 'animated': <if it is part of an animation which means whether is a frame of an animation> , 'active':<whether is the active frame, the inactive frames are not visible by default by that is usually up to the morph to decide depending on its function and intention>}"""
    def __init__(self,name="", texture = None):
        # naming is optinal but could be used as an id
        self._name = name
        
        # name: name of png files
        # visible: visibility of each png file, either True or False
        # animated: is that layer/png part of the animation ?
        # active: is it the active frame
        # eg:
        # self._data = [{"name": None , "visible": False , "animated": False , "active": False , "color": None}]
        self._data = []
        
        # the texture that this instance belongs to
        self._texture = texture

        # name of active layer
        self._active_frame = 0

        # iteration index
        self._iter_index = 0

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self,value):
        self._name = value

    @property
    def frame(self):
        return self._active_frame

    @frame.setter
    def frame(self, value):
        self._active_frame = value
    
    def __repr__(self):
        output=f"<TextureLayerGroup instance: {id(self)} content:\n"
        for key in self.keys():
            output = output + "{ layer: '" + key + "' , properties: " + str(self[key]) + "}\n"
        output=output[0:-1]+">"           
        return output 

    def __len__(self):
        return len(self._layers)

    def __getitem__(self,key):
        if isinstance(key,str):
            for x in range(0, len(self._data)):
                if (self._data[x]["name"]) == key:
                    return {"visible": self._data[x]["visible"], "active": str(x == self.frame), "animated": self._data[x]["animated"], "color": self._data[x]["color"]}
            raise KeyError
        elif isinstance(key,int):
            if key < len(self._data) and key > -1:
                return self._data[key]
            if key < 0 and ((key > (len(self._data)-1)*-1) or key == (((len(self._data)-1)*-1))):
                return self._data[(len(self._data)-1)+key]
            raise IndexError
        elif isinstance(key,slice):
            rl = []
            start = 0
            stop = len(self)
            step = 1
            if key.start is not None:
                start = key.start
            if key.stop is not None:
                stop = key.stop
            if key.step is not None:
                step = key. stop
            for x in range(start,stop,step):
                rl.append(self[x])
            return rl
         
    
    def __setitem__(self,key,value):
        """ assignment layer_group[key/index], can be either by key (string) liek dictionaries or index (number) either positive or negative like lists but the index like list cannot be out of bounds. For new entries to the group is layer_group + new_entry. New entry if assigned by index must be a dictionary that at least contains 'name':<name_of_png> """
        if isinstance(value,dict):
            visible = False
            animated = False
            color = [0,0,0,0]
            active = False

            if "visible" in value.keys():
                visible = value["visible"]
            if "animated" in value.keys():
                animated = value["animated"]
            if "active" in value.keys():
                active = value["active"]
            if "color" in value.keys():
                if len(value["color"]) > 4 or len(value["color"]) < 3:
                    raise ValueError(f"TextureLayerGroup<{id(self)}>: color can be of either [R,G,B] or [R,G,B,A] format")
                else:
                    color = []
                    for item in value["color"]:
                        color.append(item)
                    if len(value["color"]) == 4:
                        color.append(1)
        else:
            raise TypeError(f"TextureLayersGroup<{id(self)}>: value of layer can only be a dictionary")

        if isinstance(key,str):
            for x in range(0,len(self._data)):
                if self._data[x]["name"] == key:
                    self._data[x] = {"name": key, "visible": visible, "animated": animated, "color": color}
                    if active:
                        self.frame = x
                    return
            self._data.append({"name": key, "visible": visible, "animated": animated, "color": color})
            if active:
                self.frame = len(self._data)-1
            return
           
        elif isinstance(key,int):
            if key > 0:
                if not "name" in value.keys():
                    raise KeyError(f"TextureLayersGroup<{id(self)}>: assignment using index must be done with value dictionary containing 'name' field , for the new key")
                else:
                    name = value["name"]
                    # normal access by index
                    if key < len(self._data) and key > 0:
                        self._data[key]={"name": name, "visible": visible, "animated": animated, "color": color}
                    # cannot go beyond existing max index (last entry)
                    if key >= len(self._data):
                        raise IndexError(f"TextureLayersGroup<{id(self)}>: inex out of bounds , for a new entry use the + operator")
                    # and cannot go beyond existing min index (first entry)
                    if key <= (len(self._data)*-1):
                        raise IndexError(f"TextureLayersGroup<{id(self)}>: index out of bounds")
                    # but access otherwise by negative index works exactly the same as in lists
                    if key < 0 and key > (len(self._data)*-1):
                        self._data[len(self._data)+key]= {"name": name, "visible": visible, "animated": animated, "color": color}

        raise KeyError(f"TextureLayersGroup<{id(self)}>: key can be either a string (acess by key) or a number (access by index)")

    def __delitem__(self,key):
        if isinstance(key,str):
            for x in range(0,len(self._data)):
                if self._data[x]["name"] == key:
                    del self._data[x]
                    return
            raise KeyError(f"TextureLayersGroup<{id(self)}>: item {key} does not exist ")
        if isinstance(key, int):
            if key >= len(self._data):
                raise IndexError(f"TextureLayersGroup<{id(self)}>: deleting by index tried to access out of bounds")
            if key < 0 and key <= (len(self._data)*-1):
                raise IndexError(f"TextureLayersGroup<{id(self)}>: deleting by negative index is currently not allowed")    
            del(self._data[key])

               
    def __contains__(self,item):
        for x in range(0, len(self._data)):
            if self == item:
                return True
        return False 
    
    def __iter__(self):
        return self

    def __next__(self):
        if self._iter_index > (len(self._data)-1):
            raise StopIteration
        else:
            self._iter_index = self._iter_index + 1
            return self[self._data[self._iter_index]["name"]]

    
    def __add__(self,entry):
        """This is the normal way to append a new entry . test_layers_group1 + test_layers_group2 will add all the diffirent layers that do not exist is layer 1 to layer 1"""
        
        if isinstance(entry,__class__):
            for item in entry:
                if not item in self:
                    self[item["name"]]= entry[item["name"]]
            return 
        if isinstance(entry,dict):
            if not "name" in value.keys():
                raise ValueError(f"TextureLayersGroup<{id(self)}>: appending new entry with a dictionary must contain at least 'name':<name_of_png>")
            else:
                self._data.append(entry)
        else:
            raise TypeError(f"TextureLayersGroup<{id(self)}>: addition//appending can only take either a dictionary or an instance of TextureLayersGroup as argument")

    
    def __sub__(self,entry):
        """ this will substract/delete layer/s in common from layer 1. Either by key(string) which is the name of the png file or by index eg. layergroup - 2, will delete that last two entries. You can also remove a particular entry by key or index using del layer_group[key/index] """
        if isinstance(entry,__class__):
            for item in entry:
                if item in self:
                    del self[item["name"]]
            return
        elif isinstance(entry,int):
            if entry == 1:
                del(self._data[-1])
            else:
             del(self._data[(len(self._data)-entry):])

        else:
            raise TypeError(f"TextureLayersGroup<{id(self)}>: can only substract other insances of TextureLayersGroup, or by number of entries from end")

    
    
    def __rshift__(self,num_of_frames_to_increment):
        """ layer_group >> number , will increase active frame by a number """
        self._active_frame = self._active_frame + num_of_frames_to_increment
        if self._active_frame > (len(self._data) -1):
            self._active_frame = len(self._data) - 1
        return
    
    
    def __lshift__(self,num_of_frames_to_decrease):
        """ layer_group << number , will decrease active frame by a number """
        self._active_frame = self._active_frame - num_of_frames_to_decrease
        if self._active_frame < 0:
            self._active_frame = 0
        return
    
    
    def __eq__(self, tlg):
        """will compare two layer groups to see if they contain the same layers"""
        if hasattr(self,"_data"):
            if self._data == tlg._data:
                return True
        return False 

    def values(self):
        """ warning !!! this does not just return self._data , because this class behaves like a dictionary , self._data["name"] is ommited from the returned dictionary because it is the key to the dictionary of this texture layer group."""
        rdict = {}
        for key in self.keys():
            rdict.append(self[key])
        return rdict.values()
  
    def keys(self):
        """simple index names returns, which is of course layer names """
        names = []
        for x in range(0,len(self._data)):
            names.append(self._data[x]["name"])
        return names

    



class Texture(pylivecoding.LiveObject):
    texture_default_path = "/hal/icons/"

    def __init__(self, filename = "", is_active = False):
        self.layers =[]
        self.layer_visibility = []
    
    def add_layer(self,filename, visibility = False):
        for f in self.layers:
            if filename == f:
                return False
        self.layers.append(filename)
        self.layer_visibility.append(visibility)
        return True
    
    def remove_layer(self,filename):
        for x in range(0,len(self.layers)):
            if filename == self.layers[x]:
                del self.layers[x]
                del self.layer_visibility[x]
                return self
        raise ValueError("Hecate: cannot find the texture layer to be removed")
    
    def is_layer_visible(self,filename):
        for x in range(0,len(self.layers)):
            if filename == self.layers[x]:
                return self.layer_visibility[x]
    
    def make_layer_visible(self,filename):
        for x in range(0,len(self.layers)):
            if filename == self.layers[x]:
                self.layer_visibility[x] = True
                return
        raise ValueError("Hecate: cannot find the texture layer to make visible")
    
    def make_layer_invisible(self,filename):
        for x in range(0,len(self.layers)):
            if filename == self.layers[x]:
                self.layer_visibility[x] = False
                return
        raise ValueError("Hecate: cannot find the texture layer to make invisible")
    