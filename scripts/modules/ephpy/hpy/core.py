"""
IGNORE:

    /*
    * ***** BEGIN GPL LICENSE BLOCK *****
    *
    * This program is free software; you can redistribute it and/or
    * modify it under the terms of the GNU General Public License
    * as published by the Free Software Foundation; either version 2
    * of the License, or (at your option) any later version.
    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software Foundation,
    * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    *
    * Developer: Dimitris Chloupis
    * 
    * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
    * All rights reserved.
    * ***** END GPL LICENSE BLOCK *****
    */
IGNORE

"""

import os,bpy,time

from ephpy import pylivecoding

from . import event as hec_event
from . import texture as hec_texture
from . import layout_engine as hec_layout

import pdb

class Morph(pylivecoding.LiveObject):
    """The simplest GUI element
    
    The Morph is extremely essential in Hecate. It provides the base class that  all other morph classes are inherit from. In this class is the most basic functionality of GUI elements that are called "Morphs". Specific functionality  is defined on specific subclasses of this class. Hecate is inspired by Morphic, a GUI that is also based on morphs, approaching GUI creation as a lego like process  of assembling almost identical things together with ease and simplicity 
    
    .. inheritance-diagram:: ephpy.hpy.core.Morph
        :parts: 1
    
    .. autosummary::
       
        world
        add_morph
        children
        parent
        position
        world_position
        absolute_position
        width
        height 
        text
        info_text
        texture
        colapse
        expand
        mouse_over_morph
        append_to_catalog
        is_hidden
        draw
        x
        y

    """
       
    def __init__(self, texture="", textures=[""], width=36, height=36, position=[0,0], 
    color=[1.0, 1.0, 1.0, 1.0], name='noname', on_left_click_action=None, on_left_click_released_action=None,on_right_click_action=None, on_right_click_released_action=None, on_mouse_in_action=None, on_mouse_out_action=None, on_drag_action=None, on_drop_action=None, texture_path=None, on_draw = None,scale=1, is_container = False, info_text = ""):
        """
        Creates the morph. A morph is the most basic graphical element, in this case it represents just an image on screen. A morph is made of textures. A moprh can have multiple textures but only one texture active a time defined by self.active_texture. A texture itself is a collection of layers semi transparent images.

        :param str texture: the name of the icon to be used, see also :py:attr: `~texture`
        :param list(str) textures: more than one texture to be used
        :param int width: width of the morph
        :param int height: height of the morph
        :param list[int,int] position: position of the morph
        :param list[float,float,float,float] color: color of the morph [r,g,b,a]
        :param str name: name of the morph used to identify and locate specific morphs
        :param method([self,]morph) on_left_click_action: a method with signature (self,morph) to be executed when users left clicks on morph
        :param method([self,]morph) on_left_click_released_action: a method to be executed when users released left click on morph
        :param method([self,]morph) on_right_click_action: a method to be executed when users right clicks on morph
        :param method([self,]morph) on_right_click_released_action: a method to be executed when users released right click on morph
        :param method([self,]morph) on_mouse_in_action: a method to be executed when users enters area of morph with mouse
        :param method([self,]morph) on_mouse_out_action: a method to be executed when users exits area of morph with mouse
        :param method([self,]morph) on_mouse_drag_action: a method to be executed when users drags the morph with mouse
        :param method([self,]morph) on_mouse_drops_action: a method to be executed when users ends draging the morph with mouse
        :param method([self,]morph) on_mouse_draw_action: a method to be executed when morph is about to draw itself
        :param str texture_path: the path where the png file can be located
        :param int scale: a number that can be used to multiple width and height
        :param bool(default=false) is_container: whether morph acts as a container for other morphs 
        :param str info_text: text usually displayed by tooltip when mouse hovers over morph

        start with creating the morph

        >>> m = Morph()
        >>> print(m.width)
        36

        """
       
        self._is_enabled=False 
        
        self._text =""
        # a morph can be inside another morph. That other morph is the parent while this morph becomes the child
        self._parent = None
        self._children = []

        self._width = 0
        self.width = width
        self._height = 0
        self.height = height
        self._position = position

        
        self._info_text = info_text

        self.is_container = is_container
        self.is_expanded = True
        self._width_old = self.width
        self._height_old = self.height
        self.main_morph = None
        self.catalogue_entry_type = "default"

        self.on_draw = on_draw
        
        # event_processor handles events for morph
        self.event_processor = hec_event.EventProcessor( on_left_click_action = on_left_click_action,
        on_left_click_released_action = on_left_click_released_action,
        on_right_click_action = on_right_click_action,
        on_right_click_released_action = on_right_click_released_action,
        on_mouse_in_action = on_mouse_in_action,
        on_mouse_out_action = on_mouse_out_action, on_drag_action=on_drag_action, on_drop_action = on_drop_action)

        self.event_processor.morph= self

        self.color = color
        
        self._is_hidden = False

        # a world is essentially a parent without a parent. World is a morph responsible for
        # anything that is not morph specific and general functionalities like figuring out
        # where the mouse is and what region draws at the time
        self._world = None

        # a name is an optional feature for when you want to locate a specific morph inside a world
        # and do something to it or do something with it
        self._name = name

        # this counts the amount of times the morph has been drawn. Can be useful to figure out FPS
        # and make sure Hecate does not slow down Blender
        self.draw_count = 0
        
        # a morph can be scaled like any blender object. The scale is also tied to the scale of the active texture
        # the scale of the active texture depends on the dimensions of the png file
        self._scale = scale

        # this tells where to find the textures
        if texture_path is None:
            self.texture_default_path = hec_texture.Texture.texture_default_path
        else:
            self.texture_path = texture_path
  
        # active texture is the texture displaying at the time
        # only one texture can display at a time , if you want more then you have to have multiple child morphs
        # each child will have its own active texture
        if texture != "" and texture != None:
            tex = hec_texture.Texture()
            tex.add_layer(texture,visibility = True)
            self.active_texture = tex
        else:
            self.active_texture = hec_texture.Texture()

        """ """
        if textures != [""]:
            for t in textures:
                self.textures=[]
                tex = hec_texture.Texture()
                tex.add_layer(t)
                self.textures.append(tex)
        else:
            self.textures = [self.active_texture]
                        
        self.can_draw = True
        self._can_hide = True

    @property 
    def children(self):
        """ returns and sets the children of a morph

        when :py:meth:`add_morph` is used , a child is added to :py:meth:`children` and that child morph has its :py:meth:`parent` set to self.

        :param list(Morph) value: list of morphs as children
        :return: children morphs
        :rtype: list(Morph)
        """ 
        return self._children
    
    @children.setter
    def children(self,value):
        self._children = value
    
    @property
    def text(self):
        """this is a :py:class:`str` value that is most frequently used by :py:class:`TextMorph` to determine the text to be displayed but other subclasses could use it as well. """
        return self._text
    
    @text.setter
    def text(self,value):
        self._text = value

    @property
    def info_text(self):
        """used by tooltip to descibe the focused morph"""
        return self._info_text

    @info_text.setter
    def info_text(self,value):
        self._info_text = value

    @property
    def is_enabled(self):
        """ (bool) mainly used by :py:class:`ButtonMorph` and all its subclasses to keep the highlight on"""  
        return self._is_enabled

    @is_enabled.setter
    def is_enabled(self,value):
        self._is_enabled = value

   
    @property
    def texture(self):
        """the active texture
        
        contains a reference to an instance of :py:class:`ephpy.hpy.texture.Texture` as the active texture used by this morph
         """
        return self.active_texture
       
    @texture.setter
    def texture(self, texture):
        self.active_texture = texture
        for tex in self.textures:
            if tex is texture:
                return "texture_found"
        self.textures.append(texture)
        return "texture_appended"

    @property 
    def can_hide(self):
        return self._can_hide
    
    @can_hide.setter
    def can_hide(self,value):
        self._can_hide = value

    # width of the morph
    @property
    def width(self):
        """width of morph
        
        can only be a positive number
        
        :param int value: width
        :return: width
        :rtype: int
        """
        if self._width < 0:
            raise ValueError("width must not be a negative value")
        else:
            return self._width

    @width.setter
    def width(self, value):
        if value < 0:
            raise ValueError("new value for width must be a positive number")
        else:
            self._width = value
            if self.parent is not None:
                if (self.position[0] + self.width) > self.parent.width:
                    offset = (self.position[0] + self.width) - self.parent.width
                    self.parent.width = self.parent.width + offset 

    # height of the morph
    @property
    def height(self):
        """height of morph
        
        can only be a positive number
        
        :param int value: height
        :return: height
        :rtype: int
        """
        if self._height < 0:
            raise ValueError("height must not be a negative value ")
        else:
            return self._height

    @height.setter
    def height(self, value):
        if value < 0:
            raise ValueError("new value for width must be a positive number")
        else:
            self._height = value
            if self.parent is not None:
                if (self.position[1] + self.height) > self.parent.height:
                    offset = (self.position[1] + self.height) - self.parent.height
                    self.parent.height = self.parent.height + offset 

    
    @property
    def position(self):
        """ position is relative to its parent morph

        it can get or set position, if the new position is outside the parent's bounds then it will enlarge the parent bounds so the parent is big enough to cover the area of the child

        :param list value: [x,y] position to set
        :return: position [x,y]
        :rtype: list

        if positioned outside the boundaries of its parent, parent expands to include the child in its area

        >>> m = Moprh()
        >>> y = Morph()
        >>> m.add_morph(y)
        >>> y.position=[300,0]
        >>> m.width 
        336
        
        otherwise it keeps its standard width
        
        >>> m.width = 36
        >>> y.position=[0,0]
        >>> m.width
        36
        """
        return self._position

    @position.setter
    def position(self, value):
        if value[0] < 0 or value[1] < 0 :
            raise ValueError("new position must be a list with positive values")
        else:
            if self.parent is not None:
                if (value[0] + self.width) > self.parent.width:
                    self.parent.width = value[0] + self.width
                if (value[1] + self.height) > self.parent.height:
                    self.parent.height = value[1] + self.height
            self._position = value


    # world position returns the position of the morph relative to the world it belongs too
    @property
    def world_position(self):
        if self.parent is not None:
            return [self.parent.world_position[0] + self.position[0],
                    self.parent.world_position[1] + self.position[1]]
        else:
            return self.position

    # world position is a read only variable
    @world_position.setter
    def world_position(self, value):
        raise ValueError("world_position is read only !")

    # absolute position is the position relative to the entire blender window
    @property
    def absolute_position(self):
        if self.world is not None:
            return [self.world_position[0] + self.world.position[0] , self.world_position[1]+ self.world.position[1] ]
        else:
            return self.world_position

    # world position is a read only variable
    @absolute_position.setter
    def absolute_position(self, value):
        raise ValueError("absolute_position is read only !")

    # bounds defines the boundary box of the morph
    @property
    def bounds(self):
        return [self.position[0], self.position[1], self.position[0] + self.width,
                       self.position[1] + self.height]
    @bounds.setter
    def bounds(self,value):
        raise ValueError("bounds is read only !")
        
    @property
    def mouse_over_morph(self):
        "checks to see whether the mouse if over this morph and returns either True or False. This property is **ready only**"
        apx1 = self.absolute_position[0]
        apy1 = self.absolute_position[1]
        apx2 = self.absolute_position[0] + self.width
        apy2 = self.absolute_position[1] + self.height
        ex = self.world.mouse_position[0]
        ey = self.world.mouse_position[1]
        result = ( ex > apx1 and ex < apx2 and ey > apy1 and ey < apy2)
        return result
    
    # every Morph belongs to a World which is another Morph
    # acting as a general manager of the behavior of Morphs
    @property
    def world(self):
        """ returns or assigns instance of the :py:class:`World` to which this morph belongs"""
        if self._world is None and self._parent is not None:
            self._world = self.parent.world
        return self._world

    @world.setter
    def world(self, value):
        self._world = value

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self,value):
        self._scale = value
        self.width = round(self.width * value)
        self.height = round(self.height * value)
        for morph in self.children:
            morph.scale = value

    # a Morph can contain another Morph, if so each morph it contains
    # is called a "child" and for each child it is the parent
    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    @property
    def is_hidden(self):
        return self._is_hidden

    @is_hidden.setter
    def is_hidden(self, value):
        if self.can_hide:
            self._is_hidden = value
        elif value:
            raise ValueError("Hecate: cannot hide a morph if morph.can_hide is False")
        elif not value:
            self._is_hidden = value
        else:
            raise ValueError("Hecate: morph.is_hidden can only accept True or False values")


    @property
    def name(self):
        return self._name

    @name.setter
    def name(self,new_name):
        self._name = new_name

    # collapse morph if it is a container
    def colapse(self):
        """this is used only for morphs that are containers. It minimises the moprh by reducing its width and height to its first child which is also the only child that remains visible. If you wish to completely hide the container morph thet you should use :py:attr:`~.is_hidden`"""
        if self.main_morph is not None and self.is_container:
            self._height_old = self.height
            self._width_old = self.width
            self.width = self.main_morph.width + self.main_morph.position[0]
            self.height = self.main_morph.height + self.main_morph.position[1]
            for child in self.children:
                if child is not self.main_morph:
                    child.is_hidden = True
            self.is_expanded = False
            return "success"
        return "failure"
    
    # expand morph if it is a container
    def expand(self):
        """this is used only for moprhs that are containers. It maximises the size of the container morph making all the moprh it contains visible. The moprh it contains are its children.""" 
        if self._height_old is not None and self.is_container:
            self.height = self._height_old
        if self._width_old  is not None:
            self.width = self._width_old
        for child in self.children:
                child.is_hidden = False
        self.is_expanded = True
        return "success"
    
    # one texture can be active at the time in order to display on screen
    def activate_texture_index(self, index):
        """this method activates a specific texture from a specific index of the textures list.

        :type index: integer
        :param index: a number that is the index of the texture to activate. Only one texture can be active at a time"""
        if index < len(self.textures):
            self.active_texture = self.textures[index]
        else:
            raise ValueError("Hecate: texture index given is too large")
        return "success"
    
    def activate_texture(self,texture):
        """this method uses a texture as the active texture referenced by :py:attr:`~.active_texture`, and adds it to :py:attr:`~.textures` list.

        :type texture: Texture
        :param texture: the texture to be used as active texture  """
        for tex in self.textures:
            if tex is texture:
                self.active_texture = texture
                return self
            else:
                raise ValueError("Hecate: texture cannot be found")
        return "success"
    
    def add_texture(self,texture):
        """this method adds a texture to :py:attr:`~.textures` list

        :type texture: Texture
        :param texture: the texture to be added  """
        for tex in self.textures:
            if tex is texture:
                raise ValueError("Hecate: duplicate texture")
        self.textures.append(texture)
        return "success"

    
    def remove_texture(self,texture):
        """finds the texture in :py:attr:`.textures` list and removes it

        :type texture: Texture
        :param texture: the texture to be removed"""
        is_a_texture_deleted = False
        for tex in self.textures:
            if tex is texture:
                del tex
                is_a_texture_deleted = True
        if is_a_texture_deleted:
            return "success"
        else:
            raise ValueError("Hecate: could not locate the texture to be removed from textures")       

    
    def add_morph(self, morph):
        """add a morph as a child 

        the child makes self as parent Morph

        :param Morph morph: the morph to be added as child
        
        add a child morph to it

        >>> m = Morph()
        >>> y = Morph()
        >>> m.add_morph(y)
        >>> y.parent is m
        True

        """

        morph.parent = self
        morph.world = self.world
        self.children.append(morph)
        

    # same as add_morph the only diffirence is that it can add a morph at a specific index
    # so that this way you can define which morph comes first, which one last etc. This matters
    # because the drawing happens in layers so one will overlap the other which allows you to take
    # advantage of the transparency of the image files used for the layers of the active texture
    # for each morph and its children

    def add_morph_at(self,index,morph):
        morph.parent = self
        morph.world = self.world
        self.children.insert(index,morph)

    # this method can be overriden to do special actions during drawing morphs
    def draw(self):
        if self.on_draw is not None:
            self.on_draw(self)
        if not self.is_hidden and self.can_draw and not self.parent.is_hidden and not self.world.is_hidden:
            self.append_to_catalog()
        for morph in self.children:
            morph.draw()
        return

    # internal method , don't use uless you understand what you are doing
    # this method will trigger the Hecate ACL mechanism for loading the images 
    # defined as layers for the active_texture as long as the layer is visible.
    # There can be only one active texture at a time. 
       
    def append_to_catalog(self):
        if not self.is_hidden and self.can_draw:
            for x in range(0, len(self.active_texture.layers)):
                if self.active_texture.layer_visibility[x]: 
                    self.world.modal_operator.ephestos_component.request("hec_morph_catalog_append",[self.world_position[0],self.world_position[1],self.width,self.height,self.active_texture.layers[x],self.catalogue_entry_type,2,"",self.text])
        return "success"

    # returns a child morph of a specific name of course this depend on the definition
    # of a name at the creation of Morph or after
    def get_child_morph_named(self, name):
        if len(self.children)>0:
            for child in self.children:
                if child.name == name:
                    return child
                elif child.get_child_morph_named(name) is not None:
                    return child.get_child_morph_named(name)
        else:
            return None

    def morph_overlaps_child(self,morph):
        for morph_child in self.children:
            if morph.position[0] > morph_child.position[0] and morph.position[0] < morph_child.position[0] + morph_child.width and morph.position[1] > morph_child.position[1] and morph.position[1] < morph_child.position[1] + morph_child.height:
                return morph_child
        return None  

    # upper left corner of the bounding box
    def x(self):
        return self.position[0]

    def y(self):
        return self.position[1]

    # lower right corner of the bounting box, defining the area occupied by the morph
    def x2(self):
        return self.x() + self.width

    def y2(self):
        return self.y() + self.height   


class World(Morph):
    """World is a main container and manager for all morphs

    A World morph instance is a morph that is the parent of all top parent morphs and as such it acts as the container of all morphs. It's main purpose to make sure morphs are in their right position , are drawn properly and their events are handled. If an event is not consumed by a morph then it is returned back to Blender to be handled by Blender's normal functionalities depending on the event. If a event is consumed by a morph , which means it uses it for its internal functionality, then the even is not returned back to Blender and as such for Blender will be as the event never happened. Morph generally handle mouse events like left and right mouse clicks as well as mouse movement, but they are capable of handling keyboard events as well.

    A morph returns and assign its world via :py:attr:`Morph.world`
    
    A world instance is simple morph that triggers and handles the drawing methods and event methods for each child morph. In order for a morph to be a child of a World it has to be added to it or else it wont display. There can be more than one world. Generally this is not necessary if you want to create a multi layer interfaces because each morph can act as a container (parent) to other morphs (children). On the other hand there are cases when you want each layer to be really separate and with its own handling of events and drawing which make sense to have multiple worlds. The choice is up to you but remember you have to call on_event method for each world you have if you want that world to display and handle events for its children morphs.A world requires a modal operator , because only Blender's modal operators are the recommended way for handling Blender events and drawing on regions of internal Blender windows. As such the draw() method must be called inside the method associated with the modal's drawing and on_event is called on the modal method of your modal operator. You need to call only those two methods for Hecate to work of course taking into account you have already created a world , creted the morphs and added the morphs to the world via add_morph method.
    
    .. inheritance-diagram:: ephpy.hpy.core.World
        :parts: 1

    """
    def __init__(self,*args, **kwargs):
        """create the world see :py:class:`Morph.__init__` for more details """
        super().__init__(*args,**kwargs)

        # this defines whether the event send to World's onEvent method
        # has been handled by any morph. If it has not , you can use this variable
        # to make sure your modal method returns {"PASS_THROUGH"} so that the event
        # is passed back to Blender and you don't block user interaction
        self.consumed_event = False

        # the modal operator that uses this World
        self.modal_operator = None

        # the coordinates of the mouse cursor, its the same as blender mouse coordinates
        # of the WINDOW region of the internal window that has been assigned the modal
        # operator needed to draw and send events to Hecate. Blender does not change that
        # window, so the mouse coordinates start [0,0] does not change as well
        self.mouse_position = [0, 0]
        
        # used to store how many pixels outside the world will mouse have to be to close the menu
        self.mouse_limit = 10

        # whether mouse is inside a region that is drawing at the time
        # this is used for auto_hide feature
        self.mouse_cursor_inside = False

        self.region_height = 100
        self.region_width = 100
        self.region_position=[0,0]
    
        # the blender event as it is
        self.event = None
        self.context = None

        self.can_draw = False

        # Keep a log of anything happening with Ephestos (Hecate/Aura)
        self.log = None

        self.morph_focused = None

        self.layout_engine = None
        self.refresh_layout_on_event = True
        self.layout_refresh_rate = 0.1

        self.addon_preferences = None

        self._previous_time = None


    # world position returns the position of the morph relative to the world it belongs too
    @property
    def world_position(self):
        return [0,0]
    
    @property
    def mouse_over_morph(self):
        apx1 = self.position[0]
        apy1 = self.position[1]
        apx2 = self.position[0] + self.width
        apy2 = self.position[1] + self.height
        ex = self.mouse_position[0]
        ey = self.mouse_position[1]
        result = ( ex > apx1 and ex < apx2 and ey > apy1 and ey < apy2)
        return result

    @property
    def height(self):
        if self._height < 0:
            raise ValueError("height must not be a negative value ")
        else:
            return self._height

    @height.setter
    def height(self, value):
        #pdb.set_trace()
        if value < 0:
            raise ValueError("new value for width must be a positive number")
        else:
            self._height = value
            if self.parent is not None:
                if (self.position[1] + self.height) > self.parent.height:
                    offset = (self.position[1] + self.height) - self.parent.height
                    self.parent.height = self.parent.height + offset 

    # a world cannot have a world by itself and of course not a parent
    # this is why we override the Morph add_morph method
    def add_morph(self, morph):
        super().add_morph(morph)
        morph.world = self
    
    def add_morph_at(self,index,morph):
        super().add_morph_at(index,morph)
        morph.world = self

    def terminate(self):
        """ Terminates the execution of the world, 
        
        usually by terminating the python modal operator accessed via self.modal_operator"""

        self.modal_operator.is_running = False
        self.generate_morph_catalog()
 


    def check_addon_preferences(self):
        """ checks whether it should refresh world
        
        It checks to see if addon preferences are enabled for hec_gui and whether it should refresh itself"""
        if self.addon_preferences is not None:
            pref = self.addon_preferences
            if pref.layout_enable:
                if pref.layout_refresh:
                    self.reflesh_layout_on_event = pref.layout_auto_refresh
                    self.layout_refresh_rate = pref.layout_refresh_rate

    def on_event(self, event, context):
        """manages blender events
        
        this is usually tied to a python modal operator which will pass the event and the context and that the world morph will administer the events to all its children which then handle them
        
        :param event event: a Blender event object 
        :param conext context: a Blender context"""
        self.check_addon_preferences()
        self.context = context
        bmx = context.region.x
        bmy = context.region.y
        self.region_position = (bmx, bmy)

        self.region_width =  context.region.width
        self.region_height = context.region.height
       
        self.event = event
    
        self.mouse_position = [event.mouse_region_x + context.region.x, event.mouse_region_y + context.region.y]
        # consume_event is reset so World does not block events that are not handled by it
        # instead those events are passed back to Blender through the {'PASS_THROUGH'} return
        # so you need to check out this variable and if it is False you need to make sure
        # the modal method of your modal operator (needed for Hecate to work)
        # returns {'PASS_THROUGH'} if you want your user to interact with
        # a Hecate GUI and Blender at the same time or else you will have an angry user hunting you down in forums
        
        self.consumed_event = False
        if not self.is_hidden:
            for morph in self.children:
                morph.event_processor.process(event, context)
            if event.type == 'LEFTMOUSE' and event.value == 'PRESS':
                self.modal_operator.log.debug(f"Was event consumed ? {self.consumed_event}")
        self.generate_morph_catalog()
            
    def keep_bounds_inside_region(self):
        """ makes sure that world snaps inside the region

        This means basically that something like the icon menu wont leave the region it was triggered from usually the 3d viewport"""

        #import pdb;pdb.set_trace()
        if self.position[0] + self.width > self.region_position[0] + self.region_width:
            old_position = [self.position[0],self.position[1]]
            self.position[0] = (self.region_position[0] + self.region_width)- self.width
           
        if self.position[1] + self.height > self.region_position[1] + self.region_height:
            self.position[1] = (self.region_position[1]+ self.region_height) - self.height

        if self.width > self.region_width:
            self.width = self.region_width
        if self.height > self.region_height:
            self.height = self.region_height
        
    # send a list of morphs to the ephestos.eac for prepartion to draw them            
    def generate_morph_catalog(self):
        """ passes info to hecate.acl to draw morphs

        this is responsible for informing at C side , via Hecate's Aura Component Library, which morphs should be drawn and which should not, to keep performance high and avoid unecessary loading of useless png files """

        if self.modal_operator is not None and self.modal_operator.is_running:
            self.keep_bounds_inside_region()
            self.modal_operator.ephestos_component.request("hec_morph_catalog_initialise",[])
            self.modal_operator.ephestos_component.request("hec_world_set",[self.position[0], self.position[1], self.width, self.height, self.region_position[0], self.region_position[1],self.region_width,self.region_height, self.mouse_position[0],self.mouse_position[1],int(self.is_hidden)])
            
            #pdb.set_trace()
            if not self.is_hidden:
                for morph in self.children:
                        #pdb.set_trace()
                    morph.draw() 
            self.modal_operator.ephestos_component.request("hec_morph_catalog_end",[])
        else:
            if self.can_hide:
                self.is_hidden = True
            self.modal_operator.ephestos_component.request("hec_morph_catalog_initialise",[])
            self.modal_operator.ephestos_component.request("hec_world_set",[self.position[0], self.position[1], self.width, self.height, self.region_position[0], self.region_position[1],self.region_width,self.region_height, self.mouse_position[0],self.mouse_position[1],int(self.is_hidden)])
            self.modal_operator.ephestos_component.request("hec_morph_catalog_end",[])
    
    

class ButtonMorph(Morph):
    """ a ButtonMorph is a morph that responds to an action. This is a default behavior for morphs, however ButtonMorph makes it a bit easier and provides an easy way to change the morph appearance when the mouse is hovering over the button"""
    def __init__(self,*args, **kwargs):
        """ creates button """
        super().__init__(*args,**kwargs)
        self.event_processor.handles_mouse_over = True
        self.event_processor.handles_events = True
        self.event_processor.handles_mouse_down = True
        self.event_processor.handles_highlight = True

    def change_appearance(self, value):
        #if "back" in self.texture:
        if value == 0 and self.active_texture:
            self.active_texture.layer_visibility[0]=True
            self.active_texture.layer_visibility[1]=False
        if value == 1 and self.active_texture:
            self.active_texture.layer_visibility[0]=False
            self.active_texture.layer_visibility[1]=True



class SwitchButtonMorph(ButtonMorph):
    """ Switch Button that can act also together with other connected morphs
    so only one of them can be active at a time"""
    def __init__(self, hover_glow_mode=True,*args, **kwargs):
        """creates switch button """
        super().__init__(*args,**kwargs)
        self.is_enabled = False
        self.connected_morphs = []
    
    def add_connection(self,morph):
        if morph.__class__ is self.__class__:
            for cmorph in self.connected_morphs:
                if cmorph is morph:
                    return "morph_already_connected"
            self.connected_morphs.append(morph)
            for cmorph in morph.connected_morphs:
                if cmorph is self:
                    return "self_already_connected"
            morph.connected_morphs.append(self)
            return "success" 
        else:
            raise ValueError("Hecate: cannot connect a SwitchButtonMorph to a morph that is not also SwitchButtonMorph")
    
    def remove_connection(self,morph):
        for con in self.connected_morphs:
            if con is morph:
                for scon in morph.connected_morphs:
                    if scon is self:
                        del scon 
                del con
   
    def enable_switch(self):
        self.is_enabled = True
        self.change_appearance(1)
        for con in self.connected_morphs:
            con.disable_switch()
    
    def disable_switch(self):
        self.is_enabled = False
        self.change_appearance(0)
        


class TextMorph(Morph):
    """a class that defines a simple label 
    
    This can be a piece of text of any size """
    def __init__(self, font_id=0, text="",size=16, dpi=72, *args, **kwargs):
        "creates the text"
        super().__init__(*args, **kwargs)
        self.size = size
        self.dpi = dpi
        self.text = text
        self.font_id = font_id
        self.text_lines = self.text.splitlines()
        self.catalogue_entry_type = "label"
    
    def append_to_catalog(self):
         self.world.modal_operator.ephestos_component.request("hec_morph_catalog_append",[self.world_position[0],self.world_position[1],self.width,self.height,"",self.catalogue_entry_type,2,"",self.text])

           
class KnobMorph(ButtonMorph):
    """ a knob is basically a cicrular slider
    
    Inspired by knobs found in audio hardware and software """
    def __init__(self, font_id=0, text="",size=16, dpi=72, *args, **kwargs):
        "creates a knob morph"
        super().__init__(*args, **kwargs)
        old_event_processor = self.event_processor
        self.event_processor = hec_event.KnobEventProcessor()
        self.event_processor.clone(old_event_processor)
        del old_event_processor
        self.event_processor.handles_events = True
        self.event_processor.handles_drag_drop = True
        self.event_processor.handles_mouse_down = True
        self.event_processor.handles_mouse_over = True
        self.event_processor.handles_highlight = True
        
         # value controled by the knob
        self._value = 0
       
        # value multiplier when we we want the knob to increase faster or slower it can also have negative values if we want to reverse the behavior of the knob
        self._value_increment_step = 1
       
        # use value None to represent infinity for min and max values
        self._max_value = None
        self._min_value = None
    
        # here we define which Blender property the knob controls , what kind of object it is and whether it has to be already selected or activated by the user
        self.controlled_property = None
        self.controlled_object = None
        self.has_to_be_selected = False
        self.has_to_be_active = False
    
    @property
    def max_value(self):
        return self._max_value
    
    @max_value.setter
    def max_value(self,val):
        self._max_value = val

    @property
    def min_value(self):
        return self._min_value
    
    @min_value.setter
    def min_value(self,val):
        self._min_value = val

    @property 
    def value(self):
        return self._value
    
    @value.setter
    def value(self,val):
        if self._max_value is not None and val > self._max_value:
            self._value = self._max_value
        elif self._min_value is not None and val < self._min_value:
            self._value = self._min_value
        else:
            self._value = val
    @property
    def value_increment_step(self):
        return self._value_increment_step
    
    @value_increment_step.setter
    def value_increment_step(self,value):
        self._value_increment_step = value
   
           
    def activate_texture_layer(self):
        layer = int(self.value + 2)
        self._switch_knob_texture_layer(layer)

    def _switch_knob_texture_layer(self,layer):
        if layer > 101:
            layer = 101
        if layer < 3:
            layer = 2
        if layer < 12:
            self.active_texture.make_layer_visible("/knob_bar/knob_bar_frame_000"+str(layer-2)+".png")
        else:
            self.active_texture.make_layer_visible("/knob_bar/knob_bar_frame_00"+str(layer-2)+".png")
        for x in range(0,100):
            if x != layer-2 and x < 10 and x > -10:
                self.active_texture.make_layer_invisible("/knob_bar/knob_bar_frame_000"+str(x)+".png")
            elif x!=layer-2:
                self.active_texture.make_layer_invisible("/knob_bar/knob_bar_frame_00"+str(x)+".png")

    def on_draw_default_action(self):
        #import pdb;pdb.set_trace()
        if self.controlled_object == "object":
            for ob in bpy.data.objects:
                if ob.select_get() and self.has_to_be_selected:
                    self.value = ob.location[0]
                    tooltip_focused_knob_value_morph = self.world.get_child_morph_named("focused knob value text")
                    if self.world.morph_focused is self:
                        tooltip_focused_knob_value_morph.text = str(self.value)
    def draw(self):
        if self.on_draw is not None:
            self.on_draw()
        else:
            self.on_draw_default_action()
        self.activate_texture_layer()
        if not self.is_hidden and self.can_draw:
            self.append_to_catalog()
        for morph in self.children:
            morph.draw()
        
       
        
    
class TooltipMorph(KnobMorph):
    """ a morph that acts as a tooltip"""
    def __init__(self, font_id=0, text="",size=16, dpi=72, *args, **kwargs):
        """creates a tooltip"""
        super().__init__(*args, **kwargs)
        self.event_processor = hec_event.TooltipEventProcessor()
        self.event_processor.morph = self
        self.is_hidden = False
        self.can_draw = True
        self.text = ""
        self.mode = "passive"
    
    def append_to_catalog(self):
        if not self.is_hidden and self.can_draw:
            for x in range(0, len(self.active_texture.layers)):
                if self.active_texture.layer_visibility[x]: 
                    self.world.modal_operator.ephestos_component.request("hec_morph_catalog_append",[self.world_position[0],self.world_position[1],self.width,self.height,self.active_texture.layers[x],"default",2,"",""])
            self.world.modal_operator.ephestos_component.request("hec_morph_catalog_append",[self.world_position[0],self.world_position[1]+10,150,18,"","label",2,"",self.text])
        return "success"
    
    def draw(self):
        if self.can_draw and not self.is_hidden:
            self.append_to_catalog()
        for morph in self.children:
                morph.draw()
class LineBarMorph(KnobMorph):
    """part of TooltipMorph
    
    it displayes a horizontal slider for knob morphs"""
    def __init__(self, font_id=0, text="",size=16, dpi=72, *args, **kwargs):
        "creates the line bar morph"
        super().__init__(*args, **kwargs)
        self.event_processor = hec_event.EventProcessor()
        self.event_processor.morph = self
        self.is_hidden = True
        self.can_draw = True
        self.parent_initial_width = 0
        
    def activate_texture_layer(self):
        if self.world.morph_focused is not None:
            if hasattr(self.world.morph_focused,"value"):
                layer = int(self.world.morph_focused.value)
                self.is_hidden = False
                if self.parent_initial_width != 0:
                    self.parent.width = self.position[0]+self.width
            else:
                self.is_hidden = True
                if self.world.layout_engine is not None:
                    for morph_dict in self.world.layout_engine.json_content["morphs"]:
                        if "name" in morph_dict.keys():
                            if self.parent.name==morph_dict["name"]:
                                self.parent.width = morph_dict["width"]
                                self.parent_initial_width = self.parent.width              
                layer = 0
        else:
            layer = 0
        self._switch_knob_texture_layer(layer)
        

    def _switch_knob_texture_layer(self,layer):
        if layer > 100 or layer < -100:
            layer = 100
        if layer < 0:
            layer = layer * -1
        if layer < 10:
            self.active_texture.make_layer_visible("/line_bar/line_bar_frame_000"+str(layer)+".png")
        elif layer == 100:
            self.active_texture.make_layer_visible("/line_bar/line_bar_frame_0"+str(layer)+".png")
        else:
            self.active_texture.make_layer_visible("/line_bar/line_bar_frame_00"+str(layer)+".png")
        for x in range(0,101):
            if x != layer and x < 10:
                self.active_texture.make_layer_invisible("/line_bar/line_bar_frame_000"+str(x)+".png")
            elif x != layer and x == 100:
                self.active_texture.make_layer_invisible("/line_bar/line_bar_frame_0"+str(x)+".png")
            elif x!=layer:
                self.active_texture.make_layer_invisible("/line_bar/line_bar_frame_00"+str(x)+".png")
            
    
    def draw(self):
        if self.on_draw is not None:
            self.on_draw(self)
        self.activate_texture_layer()
        if not self.is_hidden and self.can_draw:
            self.append_to_catalog()
        for morph in self.children:
            morph.draw()
        
        
    
       
