import unittest
import sys

class TestStringMethods(unittest.TestCase):

    
    def test_upper(self):
        self.longMessage = False
        self.assertMultiLineEqual('foo'.upper(), 'FOOG',msg="test_upper .........failed")

    def test_isupper(self):
        self.assertFalse('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    loader = unittest.defaultTestLoader
    test = loader.loadTestsFromTestCase(TestStringMethods)
    suite = unittest.TestSuite(test)
    res = unittest.TestResult()
    suite.run(res)
    print("amount of failed tests : ", len(res.failures))
    print("failures : ",res.failures)
    print("failed class name :",res.failures[0][0])
    print("tests run : ",res.testsRun)
    print("method name : ",res.failures[0][0]._testMethodName)
    print("message : ",res.failures[1][1])
    print(res.failures[0][0].id())
    #unittest.TextTestRunner().run(suite)
    
