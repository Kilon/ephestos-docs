import unittest
from .. import core

class TestMorph(unittest.TestCase):

    def test_creation(self):
        morph = core.Morph()
        self.assertIsInstance(morph,core.Morph)
        self.assertEqual(morph.position,[0,0])
        self.assertIsNone(morph.parent)
        self.assertIsNone(morph.world)
    
    def test_parenting(self):
        morph = core.Morph()
        child = core.Morph()
        morph.add_morph(child)
        self.assertIs(child,morph.children[0])
        self.assertIs(morph, child.parent)
        self.assertIsNone(morph.world)
        self.assertIsNone(child.world)
    
    def test_position(self):
        morph = core.Morph()
        child = core.Morph()
        morph.add_morph(child)
        morph.position=[100,100]
        self.assertEqual(child.position,[0,0])
        self.assertEqual(child.world_position,[100,100])
        self.assertEqual(child.absolute_position,[100,100])
        self.assertEqual(morph.position,morph.world_position)

    def test_world_position(self):
        morph = core.Morph()
        child = core.Morph()
        morph.add_morph(child)
        morph.position=[100,100]
        world = core.World()
        self.assertEqual(world.position,[0,0])
        world.position = [20,20]
        world.add_morph(morph)
        self.assertIs(morph.world,world)
        self.assertIs(child.world,world)
        self.assertEqual(morph.position,[100,100])
        self.assertEqual(morph.world_position,[100,100])
        self.assertEqual(child.position,[0,0])
        self.assertEqual(morph.absolute_position,[120,120])
        self.assertEqual(child.absolute_position,[120,120])

    def test_child_position(self):
        morph = core.Morph()
        child = core.Morph()
        morph.add_morph(child)
        morph.position=[100,100]
        world = core.World()
        world.add_morph(morph)
        child.position=[30,30]
        self.assertIs(morph.world,world)
        self.assertIs(child.world,world)
        self.assertEqual(morph.position,[100,100])
        self.assertEqual(morph.world_position,[100,100])
        self.assertEqual(child.position,[30,30])
        self.assertEqual(morph.absolute_position,[100,100])
        self.assertEqual(child.absolute_position,[130,130])

    def test_event_processor(self):
        morph = core.Morph()
        self.assertIsInstance(morph.event_processor, core.hec_event.EventProcessor)
        self.assertIs(morph,morph.event_processor.morph)




        

       

