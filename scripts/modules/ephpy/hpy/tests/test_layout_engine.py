import unittest,bpy.utils,os
from ..layout_engine import *
from .. import core
from hec_gui import morph_actions as button_actions


class TestLayoutEngine(unittest.TestCase):

    def setUp(self):
        self.layout_engine = LayoutEngine()
        self.layout_engine.load_default_layout_file("right_click_menu/hec_rcmenu_hlm.json")
        self.layout_engine.button_actions= button_actions
        self.generated_world = self.layout_engine.generate_morphs()
        

    def test_json_loading(self):
        self.assertIsNotNone(self.layout_engine.json_content)
        self.assertIsInstance(self.layout_engine, LayoutEngine)
        self.assertIsInstance(self.layout_engine.json_content,dict)
        self.assertEqual(self.layout_engine.json_content["scale_x"],36)
        self.assertEqual(self.layout_engine.json_content["morphs"][0]["name"],"world")
        self.assertEqual(self.layout_engine.json_content["morphs"][0]["type"],"world")
        self.assertEqual(self.layout_engine.json_content["morphs"][0]["scale"],1)
        self.assertEqual(self.layout_engine.json_content["morphs"][2]["name"],"object_tab_container")
        self.assertEqual(self.layout_engine.json_content["morphs"][2]["parent"],"world")
        self.assertEqual(self.layout_engine.json_content["morphs"][2]["type"],"container")
        self.assertEqual(self.layout_engine.json_content["morphs"][2]["x"],0)
        self.assertEqual(self.layout_engine.json_content["morphs"][2]["y"],0)

    def test_world_generation(self):
        generated_world = self.generated_world
        self.assertIsInstance(generated_world,core.World)
        self.assertEqual(len(generated_world.children),4)
        self.assertIsInstance(generated_world.children[1], core.Morph)
        self.assertIsInstance(generated_world.children[2],core.Morph)
        self.assertIsInstance(generated_world.children[1].children[0],core.SwitchButtonMorph)
        self.assertEqual(generated_world.children[1].children[0].name,"object_tab_button")
        self.assertEqual(generated_world.children[1].children[0].info_text,"Object Mode")


        
       
        