"""/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */
"""
import unittest
from . import test_morph, test_layout_engine
from .. import core, layout_engine
class HecateTester:
    
    def __init__(self):
        self.test_loader = unittest.defaultTestLoader
        self.test_suite = None
        self.test_result = unittest.TestResult()

    def run_test(self,test_case):
        tests = self.test_loader.loadTestsFromTestCase(test_case)
        self.test_suite = unittest.TestSuite(tests)
        self.test_suite.run(self.test_result)

    def run_tests(self,test_collection):
        self.test_suite = unittest.TestSuite()
        for test_case in test_collection:
            tests = self.test_loader.loadTestsFromTestCase(test_case)
            self.test_suite.addTests(tests)
        self.test_suite.run(self.test_result)

    def run(self):
        self.run_test(test_morph.TestMorph)
        self.run_test(test_layout_engine.TestLayoutEngine)





