"""
IGNORE:
    /*
    * ***** BEGIN GPL LICENSE BLOCK *****
    *
    * This program is free software; you can redistribute it and/or
    * modify it under the terms of the GNU General Public License
    * as published by the Free Software Foundation; either version 2
    * of the License, or (at your option) any later version.
    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software Foundation,
    * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    *
    * Developer: Dimitris Chloupis
    * 
    * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
    * All rights reserved.
    * ***** END GPL LICENSE BLOCK *****
    */
IGNORE

The module that generates world via JSON

.. inheritance-diagram:: ephpy.hpy.layout_engine
    :parts: 1
   
"""
import bpy
import json,os
from ephpy import pylivecoding
from pathlib import Path
from ephpy.hpy import core

# The Layout Engine takes as input a json file and generates and world populate with morphs as defined in the JSON file
class LayoutEngine(pylivecoding.LiveObject):
    instances = []
    defaults_dir = "/2.82/datafiles/ephestos/hal/layouts/defaults/"
    user_dir = "/2.82/datafiles/ephestos/hal/layouts/user/"

    def __init__(self, world=None, addon_preferences = None):
        super().__init__()
        self.json_content = None
        self.morph_actions = None
        if world is None :
            self.generated_world = core.World()
        else:
            self.generated_world =  world
        if addon_preferences is not None:
            self.generated_world.addon_preferences = addon_preferences
        self._morphs_with_connections = {}
        self.current_morph_dict = None
        self.json_morph_dict = None    
        self.default_width = 36
        self.default_height = 36
        self.default_background = "icon_background"
        self._variables = {}
    
    @property
    def variables(self):
        return self._variables
    
    @variables.setter
    def variables(self,value_dict):
        self._variables = value_dict

    
    def get_var(self,name,alternative_value=None):
        """ find variable or else use alternative

        checks self.variables to find any JSON variables pass by the user via the JSON map file if it does not find any it returns the alternative

        :param str name: the name of the variables to search for 
        :keyword alternative_value: the alternative value to be return if the variable is not found
        :return: value of the variable or alternative value

        """

        if self.generated_world.addon_preferences is not None:
            if self.generated_world.addon_preferences.layout_default_values:
                if name == "default_width":
                    return self.generated_world.addon_preferences.layout_morph_width
                if name == "default_height":
                    return self.generated_world.addon_preferences.layout_morph_height
        if type(name) is not str:
            return alternative_value
        if name in self.variables:
            return self.variables[name]
        else:
            return alternative_value

    def extract_variables(self):
        if "variables" in self.json_content.keys():
            self.variables = self.json_content["variables"]


    def parse_action(self,md,action,morph_dict):
        """ action to do when morph is clicked 

        parses the action that can be either an assignment of a method or using one of the keyword a direct inpute of the command, usually a blender python operator, to execute

        :param dict md: the dictionary to populate to be used to generate the morphs
        :param str action: the name of the action, usually left click
        :param dict morph_dict: the JSON morph entry
        """

        if action in morph_dict.keys():
            if "default_execute_macro" in self.variables.keys():
                exec_macro = self.variables["default_execute_macro"]
                if morph_dict[action].startswith(exec_macro):
                    exec("def somemethod(morph): "+morph_dict[action].split(command_to_exec)[1]+"\nmd[action] = somemethod",globals(),locals())
                    return
            if "default_terminate_execute_macro" in self.variables.keys():
                texec_macro = self.variables["default_terminate_execute_macro"]
                if morph_dict[action].startswith(texec_macro):
                    exec("def somemethod(morph): morph.world.terminate();"+morph_dict[action].split(texec_macro)[1]+"\nmd[action] = somemethod",globals(),locals())
                    return
            if  morph_dict[action].startswith("execute:"):
                exec("def somemethod(morph): "+morph_dict[action].split("execute:")[1]+"\nmd[action] = somemethod",globals(),locals())
                return
            if morph_dict[action].startswith("terminate_execute:"):
                exec("def somemethod(morph): morph.world.terminate();"+morph_dict[action].split("terminate_execute:")[1]+"\nmd[action] = somemethod",globals(),locals())
                return
            else:
                md[action] = eval(f"self.morph_actions.{morph_dict[action].split('.')[0]}().{morph_dict[action].split('.')[1]}")
                return

    def load_default_layout_file(self, name="right_click_menu/hec_rcmenu_hlm.json"):
        """ load the JSON Layout Map File

        this is required for anything to work, it will check first the user folder and if it does not find it will search to the default folder. 

        :keyword str name: the name of the file with extension and proceeding the subfolder of either user of defaults layout subfolders
        """

        user_path = Path(os.getcwd()+__class__.user_dir+name.split("/")[0]+"/")
        names=[file.name for file in list(user_path.glob('*.*'))]
        if name.split("/")[1] in names:
            file_path = os.getcwd()+__class__.user_dir + name
        else:
            defaults_path = Path(os.getcwd()+__class__.defaults_dir+name.split("/")[0]+"/")
            names=[file.name for file in list(defaults_path.glob('*.*'))]
            if name.split("/")[1] in names:
                file_path = os.getcwd()+__class__.defaults_dir + name

        with open(file_path,encoding='utf-8-sig') as layout_file:
            layout_file_content = layout_file.read()
            self.json_content = json.loads(layout_file_content)
            self.extract_variables()
            self.default_width = self.get_var("default_width",alternative_value=36)
            self.default_height = self.get_var("default_height",alternative_value=36)
            
        
    def generate_morphs(self, world = None):
        """ generates morphs and populates world with them

        it can use an existing world or create a new one , it assumes the JSON file has been already loaded.

        :keyword World world: The world to be used, if not passed it creates and new one

        """

        for morph_dict in self.json_content["morphs"]:
            if world is None and self.generated_world is None:
                world = core.World()
                self.generated_world = world
            elif world is not None and self.generated_world is None:
                self.generated_world = world
            elif world is not None and self.generated_world is not None:
                self.generated_world = world
            elif world is None and self.generated_world is not None:
                world=self.generated_world

            if self.generated_world.addon_preferences is not None:
                if self.generated_world.addon_preferences.layout_default_values:
                    self.generated_world.mouse_limit = self.generated_world.addon_preferences.layout_world_mouse_margin

            #parse world
            if "type" in morph_dict.keys():
                if morph_dict["type"]=="world":
                    if "mouse_limit" in morph_dict.keys():
                        world.mouse_limit=self.get_var(morph_dict["mouse_limit"],alternative_value=morph_dict["mouse_limit"])
                    if "width" in morph_dict.keys():
                        world.width = self.get_var(morph_dict["width"],alternative_value = morph_dict["width"])
                    if "height" in morph_dict.keys():
                        world.height = self.get_var(morph_dict["height"],alternative_value = morph_dict["height"])
                    world.children=[]
                if world.addon_preferences is not None:
                    if world.addon_preferences.layout_default_values:
                        world.width = world.addon_preferences.layout_world_width
                        world.height = world.addon_preferences.layout_world_height

                #parse morphs with a parent that is world
                if morph_dict["type"]!="world" and world.get_child_morph_named(morph_dict["name"]) is None and morph_dict["name"] != "world" and morph_dict["parent"] == "world":
                    self.json_morph_dict = morph_dict
                    self.parse_morph_type(self.json_morph_dict,world)
                
                #parse morph with a parent that is not world
                if morph_dict["type"]!="world" and world.get_child_morph_named(morph_dict["name"]) is None and morph_dict["name"] != "world" and (morph_dict["parent"] != "world" and morph_dict["parent"] != "none"):
                    parent_morph =  world.get_child_morph_named(morph_dict["parent"])
                    if parent_morph is None:
                        raise ValueError("Layout Engine cannot find the parent morph")
                    elif isinstance(parent_morph,core.Morph):
                        self.json_morph_dict = morph_dict
                        self.parse_morph_type(self.json_morph_dict,parent_morph)    

            if "connections" in morph_dict.keys():
                self._morphs_with_connections[morph_dict["name"]]=morph_dict["connections"]

        self.generate_morph_connections()
        return world

    def parse_morph_type(self,morph_dict,parent_morph):
        self.current_morph_dict = self.prepare_morph_generation(morph_dict)
        morph_dict = self.current_morph_dict
        morph = None
        if "type" in morph_dict.keys():
            if morph_dict["type"] == "switch":
                morph = self.generate_morph_switch(morph_dict,parent_morph)
            if morph_dict["type"] == "container":
                morph = self.generate_morph_container(morph_dict,parent_morph)
            if morph_dict["type"] == "morph":
                morph = self.generate_morph(morph_dict,parent_morph)
            if morph_dict["type"] == "text":
                morph = self.generate_morph_text(morph_dict,parent_morph)
            if morph_dict["type"] == "tooltip":
                morph = self.generate_morph_tooltip(morph_dict,parent_morph)
            if morph_dict["type"] == "knob":
                morph = self.generate_morph_knob(morph_dict,parent_morph)
        if morph is not None:
            self.generate_default_bounds(morph)
    
    def parse_icon_background(self,morph,morph_dict):
        if "icon_background" in morph_dict.keys():
            morph.active_texture.add_layer(morph_dict["icon_background"]+"_dark.png",visibility=True)
            morph.active_texture.add_layer(morph_dict["icon_background"])
        else:
            morph.active_texture.add_layer(self.default_background+"_dark.png",visibility=True)
            morph.active_texture.add_layer(self.default_background+".png")
        return morph

    def prepare_morph_generation(self,morph_dict):
        """ generates the main template for the morph from JSON

        this is where common properties of the morph are precomputed from the JSON data before detecting the type and proceeding to more specific parsing.

        :param dict morph_dict: the JSON morph entry from the Layout Map File
        :return md: a dictionary populated with the JSON morph entry and anything missing with morph default values
        :rtype: dict
        """

        md = {"info":"","icon":"","width":self.default_width,"height":self.default_height,"position":[0,0],"name":'noname',"on_left_click_action":None,"on_left_click_action":None, "on_left_click_released_action":None,"on_right_click_action":None, "on_right_click_released_action":None, "on_mouse_in_action":None, "on_mouse_out_action":None, "on_drag_action":None, "on_drop_action":None, "texture_path":None, "on_draw_action":None,"scale":1, "is_container":False,"text":""}
        
        #import pdb;pdb.set_trace()
        if "type" in morph_dict.keys():
            md["type"] = morph_dict["type"]
        
        if "world" in morph_dict.keys():
            md["world"] = morph_dict["world"]

        if "parent" in morph_dict.keys():
            md["parent"] = morph_dict["parent"]

        if "name" in morph_dict.keys():
            md["name"] = morph_dict["name"]

        if "x" in morph_dict.keys():
            morph_dict["x"]=self.get_var(morph_dict["x"],alternative_value=morph_dict["x"])
            md["position"] = [morph_dict["x"] , md["position"][1]]

        if "y" in morph_dict.keys():
            morph_dict["y"]=self.get_var(morph_dict["y"],alternative_value=morph_dict["y"])
            md["position"] = [ md["position"][0], morph_dict["y"]]

        if "info" in morph_dict.keys():
            md["info"] = morph_dict["info"]

        if "icon" in morph_dict.keys():
            md["icon"] =morph_dict["icon"]+".png"

        if "icon_background" in morph_dict.keys():
            md["icon_background"] = morph_dict["icon_background"]+".png"

        if "width" in morph_dict.keys():
            if morph_dict["width"] == "world_width":
                md["width"] = self.generated_world.width
            elif morph_dict["width"] == "default_width":
                md["width"] = self.default_width
            else:
                md["width"] = morph_dict["width"]
            
        if "height" in morph_dict.keys():
            if morph_dict["height"] == "world_height":
                md["height"] = self.generated_world.height
            elif morph_dict["height"] == "default_height":
                md["height"] = self.default_height
            else:
                md["height"] = morph_dict["height"]

        if "icon" in morph_dict.keys():
            md["icon"] =morph_dict["icon"]+".png"

        if "connections" in morph_dict.keys():
            md["connections"]= morph_dict["connections"]

        if "main_morph" in morph_dict.keys():
            md["main_morph"] = morph_dict["main_morph"]

        if "text" in morph_dict.keys():
            md["text"] = morph_dict["text"]
        
        self.parse_action(md,"on_left_click_action",morph_dict)
        self.parse_action(md,"on_drag_action",morph_dict)
        self.parse_action(md,"on_draw_action",morph_dict)

        """if "drag_action" in morph_dict.keys():
            md["on_drag_action"] = eval(f"self.morph_actions.{morph_dict['drag_action'].split('.')[0]}().{morph_dict['drag_action'].split('.')[1]}")

        if "draw" in morph_dict.keys():
            md["on_draw"] = eval(f"self.morph_actions.{morph_dict['draw'].split('.')[0]}().{morph_dict['draw'].split('.')[1]}")"""

        return md

    def generate_morph(self,morph_dict,parent_morph):
 
        parent_morph.add_morph(core.Morph(name=morph_dict["name"], position=morph_dict["position"], width = morph_dict["width"] , height = morph_dict["height"], on_left_click_action=morph_dict["on_left_click_action"], info_text=morph_dict["info"]))
        parent_morph.children[-1].is_container = True
        morph = parent_morph.get_child_morph_named(morph_dict["name"])
        if "icon" in morph_dict.keys():
            morph.active_texture.add_layer(morph_dict["icon"], visibility = True)
        return morph

    def generate_morph_container(self, morph_dict,parent_morph):
        
        parent_morph.add_morph(core.Morph(name=morph_dict["name"], position=morph_dict["position"], on_left_click_action= morph_dict["on_left_click_action"]))
        parent_morph.children[-1].is_container = True
        morph = parent_morph.get_child_morph_named(morph_dict["name"])
        return morph

    def generate_morph_switch(self,morph_dict,parent_morph):
  
        parent_morph.add_morph(core.SwitchButtonMorph(name=morph_dict["name"], position=morph_dict["position"], width = morph_dict["width"] , height = morph_dict["height"], on_left_click_action=morph_dict["on_left_click_action"], info_text=morph_dict["info"]))
        morph = parent_morph.get_child_morph_named(morph_dict["name"])

        self.parse_icon_background(morph,morph_dict)      

        if "icon" in morph_dict.keys():
            morph.active_texture.add_layer(morph_dict["icon"], visibility = True)

        if "main_morph" in morph_dict.keys():
            if morph.parent.is_container:
                morph.parent.main_morph = morph
                morph.parent.colapse()

        if "connections" in morph_dict.keys():
            self._morphs_with_connections[morph_dict["name"]] = morph_dict["connections"]
        return morph
    
    def generate_morph_connections(self):
        for morph in self._morphs_with_connections.keys():
            cm= self.generated_world.get_child_morph_named(morph)
            for con in self._morphs_with_connections[morph]:
                cm.add_connection(self.generated_world.get_child_morph_named(con))

    def generate_morph_text(self,morph_dict,parent_morph):
        morph=parent_morph.add_morph(core.TextMorph(name = morph_dict["name"], texture = morph_dict["icon"], text = morph_dict["text"]))
        return morph

    def generate_morph_knob(self,morph_dict,parent_morph):
        json_morph = self.json_morph_dict
        parent_morph.add_morph(core.KnobMorph(name=morph_dict["name"], position=morph_dict["position"], width = morph_dict["width"] , height = morph_dict["height"], on_drag_action= morph_dict["on_drag_action"], on_draw=morph_dict["on_draw_action"], info_text=morph_dict["info"]))
        morph = parent_morph.get_child_morph_named(morph_dict["name"])

        self.parse_icon_background(morph,json_morph)

        if "value" in json_morph.keys():
            morph.value = json_morph["value"]
        if "max_value" in json_morph.keys():
            morph.max_value = json_morph["max_value"]
        if "min_value" in json_morph.keys():
            morph.min_value = json_morph["min_value"]
        if "object" in json_morph.keys():
            morph.controlled_object = json_morph["object"]
        if "property" in json_morph.keys():
            morph.controlled_property = json_morph["property"]
        if "step" in json_morph.keys():
            morph.value_increment_step = json_morph["step"]
        if "has_to_be_selected" in json_morph.keys():
            morph.has_to_be_selected = json_morph["has_to_be_selected"]
        if "has_to_be_active" in json_morph.keys():
            morph.has_to_be_active = json_morph["has_to_be_active"]

        for x in range(0,101):
            if x < 10:
                morph.active_texture.add_layer("/knob_bar/knob_bar_frame_000"+str(x)+".png")
            else:
                morph.active_texture.add_layer("/knob_bar/knob_bar_frame_00"+str(x)+".png")
        morph.active_texture.make_layer_visible("/knob_bar/knob_bar_frame_0000.png")
        
        if "icon" in morph_dict.keys():
            morph.active_texture.add_layer(morph_dict["icon"], visibility = True)
        return morph

    def generate_morph_tooltip(self,morph_dict,parent_morph):
        
        parent_morph.add_morph(core.TooltipMorph(name = morph_dict["name"], texture = morph_dict["icon"], text = morph_dict["text"], position = morph_dict["position"], width = morph_dict["width"] , height= morph_dict["height"]))
        morph = parent_morph.get_child_morph_named(morph_dict["name"])
        morph.add_morph(core.LineBarMorph(name="line bar",position = [150,0],width=200, height=36))
        line_bar_morph=morph.get_child_morph_named("line bar")
        line_bar_morph.add_morph(core.TextMorph(name="focused knob value text", position = [30,17], width=60 , height=16))
        for x in range(0,101):
            if x < 10:
                line_bar_morph.active_texture.add_layer("/line_bar/line_bar_frame_000"+str(x)+".png")
            elif x == 100:
                 line_bar_morph.active_texture.add_layer("/line_bar/line_bar_frame_0"+str(x)+".png")
            else:
                line_bar_morph.active_texture.add_layer("/line_bar/line_bar_frame_00"+str(x)+".png")

        line_bar_morph.active_texture.make_layer_visible("/line_bar/line_bar_frame_0000.png")
        return morph
    
    def generate_default_bounds(self,morph):
        jmd = self.json_morph_dict
        
        if not "width" in jmd.keys():
            morph.width = self.default_width
        
        if not "height" in jmd.keys():
            morph.height = self.default_height
        
        if not "y" in jmd.keys():
            if morph.parent.children[0] is not morph:
                previous_morph = morph.parent.children[-2]
                morph.position[1] = previous_morph.position[1]
            else:
                morph.position[1]=0
                
        if not "x" in jmd.keys():
            if morph.parent.children[0] is not morph:
                previous_morph = morph.parent.children[-2]
                x_position = previous_morph.position[0]+previous_morph.width
                world_x_pos = previous_morph.world_position[0]+previous_morph.width+morph.width
                if world_x_pos > morph.world.width:
                    morph.position=[0,previous_morph.position[1]+previous_morph.height]
                else:
                    morph.position[0] = x_position
            else:
                if morph.parent.is_container:
                    morph.position[0]=0
                else:
                    if (morph.position[0] + morph.parent.width) > morph.world.width:
                        morph.position=[0,morph.parent.position[1]+morph.parent.height]
                    else:
                        morph.position[0]=morph.parent.width
        
        

        




                    



       

    
