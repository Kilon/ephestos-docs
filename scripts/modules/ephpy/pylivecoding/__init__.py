"""
IGNORE:
    /*
    * ***** BEGIN GPL LICENSE BLOCK *****
    *
    * This program is free software; you can redistribute it and/or
    * modify it under the terms of the GNU General Public License
    * as published by the Free Software Foundation; either version 2
    * of the License, or (at your option) any later version.
    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software Foundation,
    * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    *
    * Developer: Dimitris Chloupis
    * 
    * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
    * All rights reserved.
    * ***** END GPL LICENSE BLOCK *****
    */
IGNORE


module for live coding with python

.. inheritance-diagram:: ephpy.pylivecoding.LiveObject ephpy.pylivecoding.LiveEnvironment
    :parts: 1


"""
import importlib,pdb,sys,traceback,inspect,re

class LiveObject:

    def __new__(cls,*args, **kwargs):
        instance = super(LiveObject,cls).__new__(cls)
        LiveEnvironment.register_class(cls.__module__,cls,instance)
        return instance
            
class LiveEnvironment(LiveObject):
    live_modules={}
    live_instances={}
    
    def register_module(name):
        if name not in LiveEnvironment.live_modules.keys():
            LiveEnvironment.live_modules[name]=[]

    def register_class(module,live_class,live_instance):
        LiveEnvironment.register_module(module)
        if not live_class in LiveEnvironment.live_modules[module]:
            LiveEnvironment.live_modules[module].append(live_class)
        if not live_class.__name__ in LiveEnvironment.live_instances.keys():
            LiveEnvironment.live_instances[live_class.__name__]=[]
        if not live_instance in LiveEnvironment.live_instances[live_class.__name__]:
            LiveEnvironment.live_instances[live_class.__name__].append(live_instance) 
    
    def update():
        for mod in LiveEnvironment.live_modules.keys(): #the name of the module is used as a key for the dict that stores all
            importlib.reload(sys.modules[mod])
            
            for x in range(0,len(LiveEnvironment.live_modules[mod])):
                old_class = LiveEnvironment.live_modules[mod][x]
                new_live_class = eval("sys.modules[\""+mod+"\"]." + old_class.__name__)
                LiveEnvironment.live_modules[mod][x]=new_live_class
                for live_instance in LiveEnvironment.live_instances[new_live_class.__name__]:
                    live_instance.__class__=new_live_class

class LiveObject2:
    def __init__(self):
        self._data= {}
    def __getattribute__(self,name):
        res = re.match(r"[^_].*", name)
        if res is not None:
            raise AttributeError(f"LiveObject<{id(self)}: direct access of private variables is not allowed")
        else:
            return object.__getattribute__(self,name)

    def gethello(self):
        print("hello there")