
===========
User Manual
===========

.. index::
    single: User Manual

Introduction
============

Welcome to Ephestos |version| , a commercial , windows only and open source (see `Blender License`_ ) 3d application  based on `Blender`_  source code.

To understand Ephestos you need to first understand the basic structure of its projects. Ephestos is a customised version of Blender source that converts Blender into a container for 6 projects. Mark in red are projects yet to be developed, marked in green are the ones that have been released. Dotted lines represent indirect dependecies (one project benefiting from another). Solid arrow direct dependencies (one project cannot work without the other) 

.. graphviz::
    :caption: Ephestos Systems Structure
    :name: ephestos-structure

    digraph ephestos {
        bgcolor = "bisque"
        node [shape=component ,labelfontsize=12, fontcolor="black"];
        ephestos [pos="2,3!",shape=tripleoctagon,label="Ephestos&#92;n[Application]", style=filled,color="yellowgreen"];
        aura [spost="2,2!",hape=folder,label="Aura&#92;n[Ephestos Plugin C API {ACLs}]",style=filled,color="yellowgreen",URL="#aura"];
        ephpy [pos="2,2!",shape = folder ,label="ephpy&#92;n[Ephestos Python API]",style=filled,color="yellowgreen"];
        hecate [pos="0,1!",label="(2) Hecate&#92;n[GUI System]",style=filled,color="yellowgreen",URL="#hecate"];
        morpheas [pos="1,1!",label="(3) Morpheas&#92;n[GUI Designer System]",style=filled,color="tomato"];
        talos [pos="0,1",label="(6) Talos&#92;n[Visual Coding System]",style=filled,color="tomato"];
        cyclops [pos="2,1!",label="(4) Cyclops&#92;n[Documentation System]",style=filled,color="tomato"];
        nyx [post="4,1!",label="(5) Nyx&#92;n[Asset Manager and Generator System]",style=filled,color="tomato"];
        
        ephestos -> aura;
        subgraph cluster_0 {
        shape=folder
		    style=filled;
		    color=seagreen;
		    node [style=filled,color=yellowgreen];
		    aura->ephpy
		    label = "(1) Aura Plugin System &#92;n[Aura Component Libraries]";
	        }
           
        ephpy -> hecate;
        hecate -> morpheas;
        hecate -> nyx;
        morpheas-> talos;
        morpheas -> cyclops;

        talos -> ephpy [style=dotted,color="navy",constrained=false];
        }

.. _aura-rules-label:

Aura
====

Aura is the main architecture of Ephestos which like a lone vigilant guardian acts as the foundation for all other systems and makes sure these systesm follow the three Ps , three simple but non compromising rules:

1. **Performance**. Everything must execute as fast as possible. Thus:

    * Aura is completely coded in C , the same programming language that Blender uses, known for producing the highest performance machine code.
    
    * Aura provides deep access to internal features of Blender, to more closely control Blender execution and general behavior that could cause any form of slow down.
    
    * Aura adds to Blender source, providing new features to enhance performance and improving over existing ones.
    
2. **Productivity**. Everything must be as easy to modify and extend as possible. Thus 

    * Aura introduces a new concept , **Aura Component Libraries**, DLLs that act very similar to addons but made entirely in C. Like addons they can be loaded and reloaded during Blender's execution. Unlike addons they can be rebuild during Blender execution and take advantage of Aura's deep access to Blender internals and high performance.
    
    * Automatic mapping of ACL functions to Python methods, through a single request function that minimises the amount of code needed to write an ACL but also ensure performance and stability. Python is closely regulated by Aura.

    * Introduction of Ephestos Python API, a collection of Python commands to easily manipulate existing ACLs. 

3. **Portability**. Everything must work as expected. Thus:

    * Aura forbids any introduction of breaking features, that may render Blender more unstable and prone to crashes.

    * File formats like Blend files, the main file format of Blender , are kept as is to ensure users can move data between Ephestos and Blender effortless.

    * Other workflows, like addons, video tutorials, UIs, shortcuts and any other additional features and tools also work exactly as expected. Even the Ephestos Python API, the main tool for the creation of Ephestos addons (not to be confused with ACLs) is also highly compatible with Blender Python API. Thus making it possible to develop addons that work both in Ephestos and Blender. Using Ephestos features **must NOT** limit existing interactions with Blender.  

Hecate
======

Hecate is the GUI system of Ephestos, unlike Aura is fully focused on user experience and offers both a Python API for easy customisation and compatibility with existing Blender addons but also a set of GUIs to enhance the user experience.

It is based on 3 principles

1. **Enhance mouse / tablet interaction**. This takes place moving away from the traditional heavily shortcut based workflow. Shortcuts are great and should be used for simple interaction but Blender is far from simple interaction and mapping advanced functionality to shortcuts is unrealistic and unproductive. This also has added advantage of not affecting existing shortcuts which fully complies with Aura's 3rd rule. 

2. **Centralise UI**. Hecate aims to bring as much of the user interaction as possible inside the 3d viewport which is already the main area of work for the vast majority of users, thus minimise the need for windows, panels , dialogs and anything that takes the focus of the user away from the main working area.

3. **Customisability**. Via Python ( :ref:`hecate-api-label` ) or JSON layout map files (Layout Engine) , Hecate GUIs are easy to customise and extend. Future systems **Morpheas** and **Talos** will takes this to the next level.Hecate focuses on the creation of tailor made UIs , the user becomes the one to decide what UI works for him/her and what it does not by being able to create new ones from scratch and modify existing ones with ease. 

.. _hecate_icon_menu:

----------------
Hecate Icon Menu
----------------

With version 0.1 , besides Aura, :ref:`hecate-api-label` and Hecate's Layout Engine, the Hecate icon menu was introduced to act as the main GUI of Ephestos 

.. figure:: icon_menu.png
    :align: center
    :alt: Hecate Icon Menu

    Hecate Icon Menu



and its preferences

.. figure:: icon_menu_preferences.png
    :align: center
    :alt: Hecate Icon Menu Preferences

    Hecate Icon Menu Preferences

Installation
============

You will need first to buy the yearly subscription from gumroad store. This will give you access to Ephesto's application and latest source code as well as a year of free updates since the day of your purchase. 

1. Download the ephestos.exe from your gumroad library
2. Double click it to decompress to a folder, navigate inside the folder and find **blender.exe**
3. Success !!! you are ready to go , zero setup required ;)
 
You are greeted with the Ephestos splash screen, that display the version of Ephestos and Blender. It is the same dialog as Blender so you will ask you to choose the theme, and keymap. Keep the Ephestos keymap it only remaps the right click for the Hecate right click menu, the rest of shortcuts are exactly the same as Blender's standard keymap. 

.. figure:: ephestos_splash_screen.png
    :align: center
    :alt: Ephestos Splash Screen Dialog

    Ephestos Splash Screen Dialog



.. _`Blender`: https://www.blender.org/ 

.. _`Blender License`: https://www.blender.org/about/license/

.. _ephestos: https://gumroad.com/l/ephestos   